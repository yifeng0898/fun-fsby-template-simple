#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.web;

import ${package}.entity.Order;
import ${package}.domain.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestMapping;



@Slf4j
@RestController
@RequestMapping("/customer")
@Tag(name = "springDoc示例接口", description = "这里是描述")
public class CustomerController {

    @Autowired
    private OrderService orderService;

    @Operation(summary = "springDoc示例接口", description = "这里是描述")
    @GetMapping(value = "/listCustomerByName")
    public void listCustomerByName(@RequestParam(required = false) String name){
        //orderService.test();
    }

}
