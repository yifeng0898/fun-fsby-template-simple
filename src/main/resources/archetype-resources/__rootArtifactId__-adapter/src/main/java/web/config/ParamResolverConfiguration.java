package ${package}.web.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Configuration
public class ParamResolverConfiguration {
    @Autowired
    private UserParamResolver userParamResolver;
    @Autowired
    private RequestMappingHandlerAdapter requestMappingHandlerAdapter;

    @PostConstruct
    public void init() {
        // 获取内建的resolvers
        List<HandlerMethodArgumentResolver> buildInList = requestMappingHandlerAdapter.getArgumentResolvers();
        // 重建resolvers
        List<HandlerMethodArgumentResolver> newResolvers = new ArrayList<>(buildInList.size() + 1);
        // 自定义的放到第一位
        newResolvers.add(userParamResolver);
        // 再放内建的
        newResolvers.addAll(buildInList);
        // 重设resolvers
        requestMappingHandlerAdapter.setArgumentResolvers(newResolvers);
    }
}
