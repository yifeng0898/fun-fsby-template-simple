#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.web;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import com.alibaba.cola.dto.SingleResponse;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



@Slf4j
@RestControllerAdvice
public class CommonAdviceController {

    /**
     * 示例
     * @param req
     * @param res
     * @param e
     * @return
     */
    @ExceptionHandler(value = NullPointerException.class)
    @ResponseBody
    public SingleResponse exceptionHandler(HttpServletRequest req, HttpServletResponse res, NullPointerException e) {
        log.error("发生空指针异常！原因是:", e);
        return SingleResponse.buildFailure("E500", "处理失败！");
    }

}
