#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.dto.data;

import lombok.Data;

@Data
public class OrderDTO{
    private Long id;
    private Integer age;
    private String name;
    private String password;
    private Integer status;
}
