#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.api;

import com.alibaba.cola.dto.SingleResponse;
import ${package}.dto.data.OrderDTO;

public interface OrderServiceClient {
    SingleResponse<OrderDTO> findOrder(Long id);
}
