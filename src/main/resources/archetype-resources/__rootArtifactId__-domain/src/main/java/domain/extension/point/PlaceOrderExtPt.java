package ${package}.domain.extension.point;

import com.alibaba.cola.extension.ExtensionPointI;

/**
 * 下单拓展点
 */
public interface PlaceOrderExtPt extends ExtensionPointI {

    Boolean place();
}
