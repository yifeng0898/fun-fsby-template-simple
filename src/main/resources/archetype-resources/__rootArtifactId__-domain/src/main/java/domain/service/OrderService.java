package ${package}.domain.service;

import com.alibaba.cola.extension.BizScenario;
import com.alibaba.cola.extension.ExtensionExecutor;
import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import ${package}.domain.extension.point.PlaceOrderExtPt;
import org.springframework.stereotype.Service;
import ${package}.domain.util.Constants;
import ${package}.entity.Order;

import java.util.concurrent.TimeUnit;
import javax.annotation.Resource;

@Service
public class OrderService {

    @Resource
    private ExtensionExecutor extensionExecutor;

    //缓存示例
    private Cache<Long, Order> cache = Caffeine.newBuilder()
            .expireAfterWrite(10, TimeUnit.MINUTES)
            .maximumSize(10_000)
            .build();

    public Order findOrderById(Long id) {
        //查询不到缓存，返回null
        Order ifPresent = cache.getIfPresent(id);
        //查找缓存，如果缓存不存在则生成缓存元素,  如果无法生成则返回null
        //Order order = cache.get(id, k -> orderGateway.findOrderById(id));
        //添加缓存
        //cache.put(id, order);
        //剔除缓存
        //cache.invalidate(id);

        //SPI动态组件调用示例，可以动态拓展业务
        Boolean result = extensionExecutor.execute(PlaceOrderExtPt.class, BizScenario.valueOf(Constants.BIZ_PLACEORDER_CUSTOM), e -> e.place());
        return null;
    };
}
