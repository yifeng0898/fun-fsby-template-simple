package ${package}.domain.eventbus.config;

import org.springframework.stereotype.Service;

import java.lang.annotation.*;

/**
 * @Desc   自动注册EvevtBus注解
 * @Author GuoFeng
 * @Date 2019/3/5
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Service
public @interface EventBusRegister {
}
