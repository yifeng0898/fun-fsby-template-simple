package ${package}.domain.extension.impl;

import com.alibaba.cola.extension.Extension;
import ${package}.domain.extension.point.PlaceOrderExtPt;
import ${package}.domain.util.Constants;
import lombok.extern.slf4j.Slf4j;

/**
 * 下单拓展实现
 */
@Slf4j
@Extension(bizId = Constants.BIZ_PLACEORDER_DEFAULT)
public class PlaceOrderExtPtImpl implements PlaceOrderExtPt {

    @Override
    public Boolean place() {
        log.info("hello");
        return null;
    }
}
