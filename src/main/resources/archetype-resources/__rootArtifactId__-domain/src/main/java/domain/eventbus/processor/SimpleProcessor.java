package ${package}.domain.eventbus.processor;

import com.google.common.eventbus.AsyncEventBus;
import com.google.common.eventbus.Subscribe;
import ${package}.domain.eventbus.config.EventBusRegister;
import ${package}.domain.eventbus.events.SimpleEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

@Slf4j
@EventBusRegister
public class SimpleProcessor {

    @Subscribe
    public void process(SimpleEvent event) {
        log.info("示例事件:{}", event);
    }



}
