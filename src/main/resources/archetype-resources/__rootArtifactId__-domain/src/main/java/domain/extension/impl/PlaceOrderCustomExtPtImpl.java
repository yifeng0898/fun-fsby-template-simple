package ${package}.domain.extension.impl;

import com.alibaba.cola.extension.Extension;
import ${package}.domain.extension.point.PlaceOrderExtPt;
import ${package}.domain.util.Constants;
import lombok.extern.slf4j.Slf4j;

/**
 * 下单拓展实现
 */
@Slf4j
@Extension(bizId = Constants.BIZ_PLACEORDER_CUSTOM)
public class PlaceOrderCustomExtPtImpl implements PlaceOrderExtPt {

    @Override
    public Boolean place() {
        log.info("custom");
        return null;
    }
}
