package ${package}.common.user;

import lombok.Data;

import java.io.Serializable;

@Data
public class UserInfo implements Serializable {

    /**
     * 用户工号
     */
    private String userCode;
    /**
     * 邮箱
     */
    private String email;
    /**
     * 用户姓名
     */
    private String userName;

}
