package ${package}.dao;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import ${package}.entity.Order;
import ${package}.repository.OrderMapper;
import org.springframework.stereotype.Service;

@Service
public class OrderDao extends ServiceImpl<OrderMapper, Order> {
}
