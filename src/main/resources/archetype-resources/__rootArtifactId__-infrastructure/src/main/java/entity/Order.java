#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

@Data
public class Order{
    @TableId(type = IdType.AUTO)
    private Long id;
    private Integer age;
    private String name;
    private String password;
    private String status;
}
