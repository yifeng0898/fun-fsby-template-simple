# fun-fsby-template
本项目是后端服务的脚手架模板工程
基于COLA架构简化之后脚手架，去掉了gateway解耦层和数据转换层等

## 介绍
项目结构是参考COLA架构来分模块的， 包括以下模块：

- controller ：适配层(最新COLA4.0中叫做Adapter)，负责对前端展示（web，wireless，wap）的路由和适配，对于传统B/S系统而言，adapter就相当于MVC中的controller；
- client ：SDK层，用于外部服务调用本项目的时候，引入本模块
- domain ：业务逻辑层，主要是封装了核心业务逻辑，并通过领域服务（Domain Service）和领域对象（Domain Entity）的方法对App层提供业务实体和业务逻辑计算。领域是应用的核心，不依赖任何其他层次；
- infrastructure ：基础设施层，主要负责技术细节问题的处理，比如数据库的CRUD、搜索引擎、文件系统、分布式服务的RPC等。此外，领域防腐的重任也落在这里，外部依赖需要通过gateway的转义处理，才能被上面的App层和Domain层使用。
- start ：项目引导模块

![本项目COLA架构示意图](cola.png)


### 使用的框架
- spring-boot:2.6.3
- spring-cloud-alibaba:2021.0.1.0
- cola:4.1.0
- mybatis-plus:3.5.2

### 软件架构
软件架构说明


### 使用说明

1.  Clone项目到本地
2.  使用命令``mvn clean install -Dmaven.test.skip=true``来打包，并安装到本地库
3.  使用命令生成项目
```bash
mvn archetype:generate  \
-DgroupId=fun.fsby.demo \
-DartifactId=demo-web \
-Dversion=1.0.0-SNAPSHOT \
-Dpackage=fun.fsby.demo \
-DarchetypeArtifactId=fun-fsby-template-simple \
-DarchetypeGroupId=fun.fsby.template \
-DarchetypeVersion=1.0.0-SNAPSHOT -DarchetypeCatalog=internal
```

4. 在start中，执行Application启动项目
 

### 项目特性
#### [MapStruct](https://mapstruct.org/)
使用MapStruct作为app层的数据转换工具

#### [lombok](https://projectlombok.org/)
使用lombok来简化POJO

#### [Guava EventBus]()
使用了Guava EventBus作为本地消息订阅工具

#### [Artahs](https://arthas.aliyun.com/)
应用默认添加了Arthas Spring Boot Starter，应用启动后，spring会启动arthas，并且attach自身进程，然后用户可以使用Arthas Web Console
地址是：http://127.0.0.1:8563/

未启用
#### 本地缓存(未启用)
##### [Caffeine](https://github.com/ben-manes/caffeine/wiki/Home-zh-CN)

### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

### 延伸阅读
[COLA 4.0：应用架构的最佳实践](https://blog.csdn.net/significantfrank/article/details/110934799)